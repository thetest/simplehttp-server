package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.io.IOUtils;
import org.fest.assertions.Assertions;
import org.fest.assertions.MapAssert;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.olamy.simplehttpserver.http.HttpMethod;
import org.olamy.simplehttpserver.http.HttpRequest;

import java.io.InputStream;
import java.util.Arrays;

/**
 * @author Olivier Lamy
 */
@RunWith( JUnit4.class )
public class BioHttpRequestParserTest
{

    protected InputStream toInputStream( String rq )
    {
        return IOUtils.toInputStream( rq );
    }

    @Test
    public void parseRequest()
        throws Exception
    {

        String rqString = "GET /foo.html HTTP/1.1\nHost: localhost:55736\n"
            + "Connection: Keep-Alive\nUser-Agent: Apache-HttpClient/4.2 (java 1.5)\n";

        BioHttpRequestParser parser = new BioHttpRequestParser();
        HttpRequest rq = parser.parse( toInputStream( rqString ) );

        Assert.assertNotNull( rq );

        Assert.assertEquals( HttpMethod.GET, rq.getMethod() );
        Assert.assertEquals( "HTTP", rq.getProtocolName() );
        Assert.assertEquals( "1.1", rq.getProtocolVersion() );
        Assert.assertEquals( "localhost:55736", rq.getHost() );
        Assert.assertEquals( "/foo.html", rq.getPath() );

        Assertions.assertThat( rq.getHeaders() ).isNotNull().isNotEmpty().hasSize( 2 ).includes(
            MapAssert.entry( "Connection", Arrays.asList( "Keep-Alive" ) ) );
    }
}
