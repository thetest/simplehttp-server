package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.HttpServerConfiguration;
import org.olamy.simplehttpserver.InvalidRequestException;
import org.olamy.simplehttpserver.SimpleHttpServerException;
import org.olamy.simplehttpserver.connector.ConnectorStatus;
import org.olamy.simplehttpserver.connector.HttpConnector;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;


/**
 * @author Olivier Lamy
 */
public class NioHttpConnector
    implements HttpConnector
{

    private Logger log = LoggerFactory.getLogger( getClass() );

    private HttpServerConfiguration httpServerConfiguration;

    private ServerSocketChannel server;

    private Selector selector;

    private int localPort;

    private final AtomicReference<ConnectorStatus> connectorStatus = new AtomicReference<ConnectorStatus>();

    private ExecutorService requestsExecutorService;

    private Thread acceptorThread;

    private SelectionKey serverKey;

    public void initialize( HttpServerConfiguration httpServerConfiguration )
        throws SimpleHttpServerException
    {
        this.httpServerConfiguration = httpServerConfiguration;

        try
        {

            server = ServerSocketChannel.open();

            server.socket().bind(
                new InetSocketAddress( httpServerConfiguration.getHost(), httpServerConfiguration.getPort() ) );

            server.configureBlocking( false );

            selector = Selector.open();

            serverKey = server.register( selector, SelectionKey.OP_ACCEPT );

            requestsExecutorService = Executors.newFixedThreadPool( httpServerConfiguration.getMaxThreads() );

            localPort = server.socket().getLocalPort();

            log.debug( "local port {}", localPort );
        }
        catch ( IOException e )
        {
            throw new SimpleHttpServerException( e.getMessage(), e );
        }


    }

    public void connect()
        throws SimpleHttpServerException
    {
        connectorStatus.set( ConnectorStatus.RUNNING );
        Runnable r = new AcceptorRunnable( this );

        acceptorThread = new Thread( r );
        acceptorThread.start();

    }

    private static class AcceptorRunnable
        implements Runnable
    {
        private NioHttpConnector nioHttpConnector;

        private Logger log = LoggerFactory.getLogger( getClass() );

        private AcceptorRunnable( NioHttpConnector nioHttpConnector )
        {
            this.nioHttpConnector = nioHttpConnector;
        }

        public void run()
        {

            while ( ConnectorStatus.RUNNING == nioHttpConnector.connectorStatus.get() )
            {
                try
                {
                    int keysNumber = nioHttpConnector.selector.select();

                    log.trace( "keysNumber: {}", keysNumber );

                    Iterator<SelectionKey> keys = nioHttpConnector.selector.selectedKeys().iterator();

                    while ( keys.hasNext() )
                    {
                        SelectionKey key = keys.next();

                        keys.remove();

                        try
                        {
                            if ( key == nioHttpConnector.serverKey )
                            {
                                log.trace( "key == nioHttpConnector.serverKey" );
                            }
                            if ( !key.isValid() )
                            {
                                log.trace( " !key.isValid" );
                                continue;
                            }

                            if ( key.isAcceptable() )
                            {
                                nioHttpConnector.acceptKey( key );
                            }
                            else if ( key.isReadable() && key.interestOps() == SelectionKey.OP_READ )
                            {
                                nioHttpConnector.readKey( key, keys );
                            }
                            else if ( key.isWritable() )
                            {
                                nioHttpConnector.writeKey( key, keys );
                            }

                        }
                        catch ( IOException e )
                        {
                            key.cancel();
                            throw new RuntimeException( e.getMessage(), e );
                        }
                    }
                }
                catch ( IOException e )
                {
                    throw new RuntimeException( e.getMessage(), e );
                }
                catch ( ClosedSelectorException e )
                {
                    // safe to ignore this on shutdown
                    if ( nioHttpConnector.connectorStatus.get() != ConnectorStatus.STOPPED )
                    {
                        log.warn( "ignore error:  " + e.getMessage(), e );
                    }
                }
            }


        }
    }

    protected void acceptKey( SelectionKey key )
        throws IOException
    {
        log.trace( "acceptKey" );
        SocketChannel clientChannel = server.accept();

        clientChannel.configureBlocking( false );

        // register the parser for reading
        readMode( clientChannel );

    }

    protected void readMode( SocketChannel clientChannel )
        throws IOException
    {
        clientChannel.register( selector, SelectionKey.OP_READ, new NioHttpRequestParser() );
    }

    protected void readKey( final SelectionKey key, final Iterator<SelectionKey> keys )
        throws IOException
    {
        log.trace( "readKey" );

        final SocketChannel clientChannel = (SocketChannel) key.channel();

        NioHttpRequestParser nioHttpRequestParser = (NioHttpRequestParser) key.attachment();

        //
        ByteBuffer requestData = ByteBuffer.allocate( 1024 );
        try
        {
            while ( clientChannel.read( requestData ) != -1 && !nioHttpRequestParser.isRequestConsumed() )
            {
                requestData.flip();
                nioHttpRequestParser.consumeData( requestData );
                requestData.clear();
            }

        }
        catch ( InvalidRequestException e )
        {
            // TODO handle invalid request
            log.warn( "InvalidRequestException: " + e.getMessage(), e );
        }

        if ( nioHttpRequestParser.isRequestConsumed() )
        {

            key.interestOps( SelectionKey.OP_WRITE );

            final HttpRequest httpRequest = nioHttpRequestParser.getHttpRequest();
            log.debug( "httpRequest: {}", httpRequest );

            new NioHttpRequestProcessor( httpServerConfiguration, key, keys, httpRequest, clientChannel ).run();

        }
    }

    protected void writeKey( final SelectionKey key, final Iterator<SelectionKey> keys )
        throws IOException
    {
        log.trace( "writeKey" );

        SocketChannel clientChannel = (SocketChannel) key.channel();

        Queue<ByteBuffer> byteBuffers = (Queue<ByteBuffer>) key.attachment();

        while ( !byteBuffers.isEmpty() )
        {
            ByteBuffer buffer = byteBuffers.poll();

            if ( buffer != null )
            {
                if ( buffer == NioHttpRequestProcessor.END_OF_RESPONSE )
                {
                    log.trace( "END_OF_RESPONSE reached" );
                    Socket socket = clientChannel.socket();
                    if ( socket.getKeepAlive() )
                    {
                        readMode( clientChannel );
                    }
                    else
                    {
                        socket.close();
                        key.cancel();
                    }
                }
                else
                {
                    clientChannel.write( buffer );
                    buffer.clear();
                }
            }
        }
    }

    public int getLocalPort()
    {
        return localPort;
    }

    public void shutdown()
        throws SimpleHttpServerException
    {

        this.connectorStatus.set( ConnectorStatus.STOPPED );

        requestsExecutorService.shutdownNow();

        acceptorThread.interrupt();

        log.info( "Server is shutting down..." );

        try
        {
            if ( selector != null && selector.isOpen() )
            {
                selector.close();
            }
        }
        catch ( IOException e )
        {
            throw new SimpleHttpServerException( "issue when shutdown server", e );
        }
        finally
        {
            try
            {
                if ( server != null && server.isOpen() )
                {
                    server.close();
                }
            }
            catch ( IOException e )
            {
                throw new SimpleHttpServerException( "isse when close sever", e );
            }
        }

    }
}
