package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.HttpServerConfiguration;
import org.olamy.simplehttpserver.SimpleHttpServerException;
import org.olamy.simplehttpserver.core.AbstractHttpRequestProcessor;
import org.olamy.simplehttpserver.http.HttpContentConstants;
import org.olamy.simplehttpserver.http.HttpHeaders;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.olamy.simplehttpserver.http.HttpResponse;
import org.olamy.simplehttpserver.http.HttpResponseBuilder;
import org.olamy.simplehttpserver.http.ResponseBodyWriter;
import org.olamy.simplehttpserver.http.ResponseStatus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.nio.ByteBuffer.allocate;

/**
 * @author Olivier Lamy
 */
public class NioHttpRequestProcessor
    extends AbstractHttpRequestProcessor
    implements Runnable
{
    public static final ByteBuffer END_OF_RESPONSE = allocate( 0 );

    private SelectionKey key;

    private Iterator<SelectionKey> keys;

    private HttpRequest httpRequest;

    private HttpResponse httpResponse = HttpResponseBuilder.newInstance( getHttpServerConfiguration() );

    private SocketChannel clientChannel;

    public NioHttpRequestProcessor( HttpServerConfiguration httpServerConfiguration, SelectionKey key,
                                    Iterator<SelectionKey> keys, HttpRequest httpRequest, SocketChannel clientChannel )
    {
        super( httpServerConfiguration );
        this.key = key;
        this.keys = keys;
        this.httpRequest = httpRequest;
        this.clientChannel = clientChannel;
    }

    public void run()
    {
        log.debug( "processrequest" );
        try
        {
            // we just check we don't have Connection: close as keep alive is default in HTTP 1.1
            if ( isKeepAlive( this.httpRequest ) )

            {
                Socket socket = clientChannel.socket();
                socket.setKeepAlive( true );
                socket.setSoTimeout( getHttpServerConfiguration().getKeepAliveTimeout() );
            }

            try
            {
                buildResponse( this.httpRequest, httpResponse );
            }
            catch ( SimpleHttpServerException e )
            {
                httpResponse.setStatus(
                    e.getResponseStatus() == null ? ResponseStatus.INTERNAL_SERVER_ERROR : e.getResponseStatus() );
            }

            writeReponse( httpResponse );
        }
        catch ( IOException e )
        {
            key.cancel();
        }
    }

    protected void writeReponse( HttpResponse httpResponse )
        throws IOException
    {

        Queue<ByteBuffer> buffers = new ConcurrentLinkedQueue<ByteBuffer>();

        // move in write
        key.interestOps( SelectionKey.OP_WRITE );

        key.attach( buffers );

        // write response status line
        StringBuilder statusLine =
            new StringBuilder( httpResponse.getProtocolName() + '/' + httpResponse.getProtocolVersion() );
        statusLine.append( ' ' + Integer.toString( httpResponse.getStatus().getCode() ) );
        statusLine.append( ' ' + httpResponse.getStatus().getText() );
        statusLine.append( HttpContentConstants.CR ).append( HttpContentConstants.LF );

        Charset utf8 = Charset.forName( "UTF-8" );

        buffers.offer( utf8.encode( statusLine.toString() ) );

        // content length header
        StringBuilder contentLength = new StringBuilder(
            HttpHeaders.CONTENT_LENGTH + HttpContentConstants.HEADER_NAME_SEPARATOR + ' '
                + httpResponse.getContentLength() );

        contentLength.append( HttpContentConstants.CR ).append( HttpContentConstants.LF );

        buffers.offer( utf8.encode( contentLength.toString() ) );

        if ( httpResponse.getContentType() != null )
        {
            StringBuilder contentType = new StringBuilder( HttpHeaders.CONTENT_TYPE ).append(
                HttpContentConstants.HEADER_NAME_SEPARATOR ).append( httpResponse.getContentType() );

            contentType.append( HttpContentConstants.CR ).append( HttpContentConstants.LF );

            buffers.offer( utf8.encode( contentType.toString() ) );
        }

        for ( Map.Entry<String, List<String>> header : httpResponse.getHeaders().entrySet() )
        {
            StringBuilder headerLine = new StringBuilder( header.getKey() ).append( ':' ).append( ' ' );
            int counter = 0;
            for ( String headerValue : header.getValue() )
            {
                headerLine.append( ( counter++ > 0 ? ", " : "" ) ).append( headerValue );
            }
            buffers.offer( utf8.encode( headerLine.toString() + HttpContentConstants.CR + HttpContentConstants.LF ) );

        }
        String bodyStartSeparator =
            new StringBuilder( HttpContentConstants.CR ).append( HttpContentConstants.LF ).toString();
        buffers.offer( utf8.encode( bodyStartSeparator ) );

        ResponseBodyWriter responseBodyWriter = httpResponse.getResponseBodyWriter();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        if ( responseBodyWriter != null )
        {
            responseBodyWriter.writeBody( byteArrayOutputStream );
        }

        byteArrayOutputStream.flush();

        buffers.offer( ByteBuffer.wrap( byteArrayOutputStream.toByteArray() ) );

        //buffers.offer( utf8.encode( bodyStartSeparator ) );

        // end
        buffers.offer( END_OF_RESPONSE );


    }
}
