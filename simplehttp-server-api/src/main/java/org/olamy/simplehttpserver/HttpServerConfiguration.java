package org.olamy.simplehttpserver;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.handler.RequestHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Olivier Lamy
 */
public class HttpServerConfiguration
{
    private int port;

    private String host = "localhost";

    private int maxThreads = 1;

    /**
     * default keep alive to 20s
     */
    private int keepAliveTimeout = 20 * 1000;

    private String httpConnectorClass;

    private String serverName = "Simple Http server";

    private List<RequestHandler> requestHandlers = new ArrayList<RequestHandler>();

    public HttpServerConfiguration()
    {
        // no op
    }

    public HttpServerConfiguration( int port, String host, int maxThreads, int keepAliveTimeout )
    {
        this.port = port;
        this.host = host;
        this.maxThreads = maxThreads;
        this.keepAliveTimeout = keepAliveTimeout;
    }

    public int getPort()
    {
        return port;
    }

    public HttpServerConfiguration setPort( int port )
    {
        this.port = port;
        return this;
    }

    public String getHost()
    {
        return host;
    }

    public HttpServerConfiguration setHost( String host )
    {
        this.host = host;
        return this;
    }

    public int getMaxThreads()
    {
        return maxThreads;
    }

    public HttpServerConfiguration setMaxThreads( int maxThreads )
    {
        this.maxThreads = maxThreads;
        return this;
    }

    public int getKeepAliveTimeout()
    {
        return keepAliveTimeout;
    }

    public HttpServerConfiguration setKeepAliveTimeout( int keepAliveTimeout )
    {
        this.keepAliveTimeout = keepAliveTimeout;
        return this;
    }

    public String getHttpConnectorClass()
    {
        return httpConnectorClass;
    }

    public HttpServerConfiguration setHttpConnectorClass( String httpConnectorClass )
    {
        this.httpConnectorClass = httpConnectorClass;
        return this;
    }

    public List<RequestHandler> getRequestHandlers()
    {
        return requestHandlers;
    }

    public HttpServerConfiguration setRequestHandlers( List<RequestHandler> requestHandlers )
    {
        this.requestHandlers = requestHandlers;
        return this;
    }

    public HttpServerConfiguration addRequestHandler( RequestHandler... requestHandlers )
    {
        this.requestHandlers.addAll( Arrays.asList( requestHandlers ) );
        return this;
    }

    public String getServerName()
    {
        return serverName;
    }

    public HttpServerConfiguration setServerName( String serverName )
    {
        this.serverName = serverName;
        return this;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append( "HttpServerConfiguration" );
        sb.append( "{port=" ).append( port );
        sb.append( ", host='" ).append( host ).append( '\'' );
        sb.append( ", maxThreads=" ).append( maxThreads );
        sb.append( ", keepAliveTimeout=" ).append( keepAliveTimeout );
        sb.append( ", httpConnectorClass='" ).append( httpConnectorClass ).append( '\'' );
        sb.append( '}' );
        return sb.toString();
    }
}
