package org.olamy.simplehttpserver.http;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Olivier Lamy
 */
public class HttpResponse
{

    private static final String DEFAULT_PROTOCOL_NAME = "HTTP";

    private static final String DEFAULT_PROTOCOL_VERSION = "1.1";

    private ResponseStatus status;

    private String protocolName = DEFAULT_PROTOCOL_NAME;

    private String protocolVersion = DEFAULT_PROTOCOL_VERSION;

    private long contentLength = 0;

    private String contentType;

    /**
     * map representing headers: key is header name and List of possible values.
     */
    private Map<String, List<String>> headers = new HashMap<String, List<String>>();

    private ResponseBodyWriter responseBodyWriter;

    public HttpResponse()
    {
        // no op
    }

    public ResponseStatus getStatus()
    {
        return status;
    }

    public HttpResponse setStatus( ResponseStatus status )
    {
        this.status = status;
        return this;
    }

    public String getProtocolName()
    {
        return protocolName;
    }

    public HttpResponse setProtocolName( String protocolName )
    {
        this.protocolName = protocolName;
        return this;
    }

    public String getProtocolVersion()
    {
        return protocolVersion;
    }

    public HttpResponse setProtocolVersion( String protocolVersion )
    {
        this.protocolVersion = protocolVersion;
        return this;
    }

    public ResponseBodyWriter getResponseBodyWriter()
    {
        return responseBodyWriter;
    }

    public void setResponseBodyWriter( ResponseBodyWriter responseBodyWriter )
    {
        this.responseBodyWriter = responseBodyWriter;
    }

    public long getContentLength()
    {
        return contentLength;
    }

    public void setContentLength( long contentLength )
    {
        this.contentLength = contentLength;
    }

    public Map<String, List<String>> getHeaders()
    {
        return headers;
    }

    public void setHeaders( Map<String, List<String>> headers )
    {
        this.headers = headers;
    }

    public List<String> getHeaderValues( String headerName )
    {
        List<String> headerValues = this.headers.get( headerName );
        return headerValues == null ? Collections.<String>emptyList() : headerValues;
    }

    public void addHeaderValue( String headerName, String headerValue )
    {
        List<String> headerValues = this.headers.get( headerName );
        if ( headerValues == null )
        {
            headerValues = new ArrayList<String>();
            this.headers.put( headerName, headerValues );
        }
        headerValues.add( headerValue );
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType( String contentType )
    {
        this.contentType = contentType;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append( "HttpResponse" );
        sb.append( "{status=" ).append( status );
        sb.append( ", protocolName='" ).append( protocolName ).append( '\'' );
        sb.append( ", protocolVersion='" ).append( protocolVersion ).append( '\'' );
        sb.append( ", contentLength=" ).append( contentLength );
        sb.append( ", contentType='" ).append( contentType ).append( '\'' );
        sb.append( ", headers=" ).append( headers );
        sb.append( ", responseBodyWriter=" ).append( responseBodyWriter );
        sb.append( '}' );
        return sb.toString();
    }
}
