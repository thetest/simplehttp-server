package org.olamy.simplehttpserver.http;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.util.HashMap;
import java.util.Map;

/**
 * @author Olivier Lamy
 */
public class HttpContentConstants
{

    public static final char CR = '\r';

    public static final char LF = '\n';

    public static final char[] CRLF = new char[]{ CR, LF };

    public static final char PROTOCOL_VERSION_SEPARATOR = '/';

    public static final char HEADER_NAME_SEPARATOR = ':';

    public static final char HEADER_VALUES_SEPARATOR = ',';

    public static final String HOST_HEADER = "Host:";

    public static final String HTTP_1_1 = "1.1";

    /**
     * content type per file extension
     */
    public static final Map<String, String> CONTENT_TYPES = new HashMap<String, String>();

    // TODO complete this list
    static
    {
        CONTENT_TYPES.put( "css", "text/css" );
        CONTENT_TYPES.put( "html", "text/html" );
        CONTENT_TYPES.put( "htm", "text/html" );
        CONTENT_TYPES.put( "js", "text/javascript" );
    }

    private HttpContentConstants()
    {
        // no constructor for this class
    }

}
