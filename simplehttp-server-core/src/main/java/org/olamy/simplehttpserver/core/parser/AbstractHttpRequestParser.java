package org.olamy.simplehttpserver.core.parser;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.lang3.StringUtils;
import org.olamy.simplehttpserver.InvalidRequestException;
import org.olamy.simplehttpserver.http.HttpContentConstants;
import org.olamy.simplehttpserver.http.HttpMethod;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Olivier Lamy
 */
public abstract class AbstractHttpRequestParser
{
    private Logger log = LoggerFactory.getLogger( getClass() );

    private HttpRequest httpRequest;

    protected AbstractHttpRequestParser()
    {
        this.httpRequest = new HttpRequest();
    }

    public HttpRequest getHttpRequest()
    {
        return httpRequest;
    }

    protected void parseLine( String line )
        throws InvalidRequestException
    {
        log.debug( "parsing line:'{}'", line );
        //request line has not been yet proceed so handle as it's always the first line
        if ( this.httpRequest.getMethod() == null )
        {

            // line format is GET /pub/WWW/TheProject.html HTTP/1.1
            String method = StringUtils.substringBefore( line, " " );
            String path = StringUtils.substringBeforeLast( line.substring( method.length() ), " " ).trim();
            String protocolNameVersion = StringUtils.substringAfterLast( line, " " );

            this.httpRequest.setMethod( HttpMethod.valueOf( method ) );
            this.httpRequest.setPath( path );
            this.httpRequest.setProtocolName( StringUtils.substringBefore( protocolNameVersion, "/" ) );
            this.httpRequest.setProtocolVersion( StringUtils.substringAfterLast( protocolNameVersion, "/" ) );
            return;
        }
        // Host is after request line
        if ( this.httpRequest.getHost() == null )
        {
            if ( StringUtils.startsWith( line, HttpContentConstants.HOST_HEADER ) )
            {
                this.httpRequest.setHost(
                    line.substring( HttpContentConstants.HOST_HEADER.length() + 1, line.length() ).trim() );
                return;
            }
        }
        // here we have headers
        String headerKey = StringUtils.substringBefore( line, ":" );
        String headerValue = StringUtils.trim( StringUtils.substringAfter( line, ":" ) );
        this.httpRequest.addHeaderValue( headerKey, headerValue );
    }

}
