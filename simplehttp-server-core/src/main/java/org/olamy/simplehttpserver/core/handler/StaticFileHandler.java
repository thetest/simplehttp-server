package org.olamy.simplehttpserver.core.handler;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.olamy.simplehttpserver.SimpleHttpServerException;
import org.olamy.simplehttpserver.handler.RequestHandler;
import org.olamy.simplehttpserver.http.HttpContentConstants;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.olamy.simplehttpserver.http.HttpResponse;
import org.olamy.simplehttpserver.http.ResponseBodyWriter;
import org.olamy.simplehttpserver.http.ResponseStatus;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Olivier Lamy
 */
public class StaticFileHandler
    implements RequestHandler
{
    private File contentDirectory;

    public StaticFileHandler( File contentDirectory )
    {
        this.contentDirectory = contentDirectory;
    }

    public boolean canServe( HttpRequest httpRequest )
    {
        return true;
    }

    public void handle( HttpRequest httpRequest, HttpResponse httpResponse )
        throws SimpleHttpServerException
    {

        switch ( httpRequest.getMethod() )
        {
            case GET:
                String filePath = httpRequest.getPath();
                File file = new File( contentDirectory, filePath );
                if ( !file.exists() )
                {
                    httpResponse.setStatus( ResponseStatus.NOT_FOUND );
                    return;
                }

                if ( file.isDirectory() )
                {
                    File indexFile = new File( file, "index.html" );
                    // if it's a directory with an index.html we serve it
                    if ( indexFile.exists() )
                    {
                        file = indexFile;
                    }
                    // whereas we build a browsing index
                    // TODO make this feature configurable
                    else
                    {
                        // TODO must be a configurable option
                        httpResponse.setStatus( ResponseStatus.OK );
                        final String directoryBrowsingContent = generateIndexBrowsing( file, httpRequest );
                        httpResponse.setContentLength( directoryBrowsingContent.length() );
                        httpResponse.setContentType( HttpContentConstants.CONTENT_TYPES.get( "html" ) );
                        httpResponse.setResponseBodyWriter( new ResponseBodyWriter()
                        {
                            public void writeBody( OutputStream outputStream )
                                throws IOException
                            {
                                IOUtils.copy( new ByteArrayInputStream( directoryBrowsingContent.getBytes() ),
                                              outputStream );
                            }
                        } );
                        return;
                    }
                }

                String fileExt = FilenameUtils.getExtension( file.getName() );

                String contentType = HttpContentConstants.CONTENT_TYPES.get( fileExt );

                httpResponse.setContentType( contentType );

                httpResponse.setStatus( ResponseStatus.OK );
                httpResponse.setContentLength( file.length() );
                httpResponse.setResponseBodyWriter( new FileResponseBodyWriter( file ) );
                return;
            default:
                // TODO handle PUT DELETE methods with body content from request
                httpResponse.setStatus( ResponseStatus.METHOD_NOT_ALLOWED );
        }


    }

    static class FileResponseBodyWriter
        implements ResponseBodyWriter
    {
        private final File file;

        private FileResponseBodyWriter( File file )
        {
            this.file = file;
        }

        public void writeBody( OutputStream outputStream )
            throws IOException
        {
            FileInputStream fis = new FileInputStream( this.file );
            IOUtils.copy( fis, outputStream );
        }
    }

    private String generateIndexBrowsing( File directory, HttpRequest httpRequest )
    {
        SimpleDateFormat sdf = new SimpleDateFormat( "dd-MMM-yyyy hh:mm" );
        StringBuilder sb = new StringBuilder();
        String path = httpRequest.getPath();
        sb.append( "<html>" ).append( "<head>" ).append( "<body bgcolor=\"white\">" ).append( "<h1>Index of " ).append(
            path ).append( "</h1>" ).append( "<hr>" ).append( "<pre>" ).append( "<a href=\"../\">../</a><br/>" );
        File[] files = directory.listFiles();
        for ( File file : files )
        {
            String cleanedPath = StringUtils.endsWith( path, "/" ) ? StringUtils.removeEnd( path, "/" ) : path;

            String name = file.getName();
            sb.append( "<a href=\"" ).append( cleanedPath ).append( '/' ).append( name ).append( "\">" ).append(
                name ).append( "/</a>" );
            if ( file.isFile() )
            {
                sb.append( sdf.format( new Date( file.lastModified() ) ) ).append( " - " ).append(
                    file.length() ).append( "<br/>" );
            }
        }
        sb.append( "</pre><hr></body></html>" );
        return sb.toString();
    }
}
