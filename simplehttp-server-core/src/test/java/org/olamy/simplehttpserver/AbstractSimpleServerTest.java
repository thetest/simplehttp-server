package org.olamy.simplehttpserver;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.fest.assertions.Assertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.olamy.simplehttpserver.connector.HttpConnector;
import org.olamy.simplehttpserver.core.handler.StaticFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author Olivier Lamy
 */
@RunWith( JUnit4.class )
public abstract class AbstractSimpleServerTest
{

    protected int localPort;

    protected HttpClient httpClient;

    protected HttpConnector httpConnector;

    protected Logger log = LoggerFactory.getLogger( getClass() );

    protected int readTimeOut = Integer.getInteger( "client.timeout", 3000 );

    @Before
    public void setupServer()
        throws Exception
    {
        HttpServerConfiguration httpServerConfiguration = new HttpServerConfiguration().setPort( 0 ).addRequestHandler(
            new StaticFileHandler(
                new File( System.getProperty( "baseDir" ), "src/test/http-server-root-directory" ) ) );

        httpConnector = getHttpConnector();
        httpConnector.initialize( httpServerConfiguration );
        httpConnector.connect();

        this.localPort = httpConnector.getLocalPort();

        this.httpClient = buidHttpClient();

        this.httpClient.getParams().setParameter( CoreConnectionPNames.SO_TIMEOUT, readTimeOut );


    }

    protected HttpClient buidHttpClient()
    {
        return new DefaultHttpClient();
    }

    @After
    public void shutdownServer()
        throws Exception
    {
        httpConnector.shutdown();
    }

    protected abstract HttpConnector getHttpConnector();

    protected abstract boolean supportKeepAlive();


    @Test
    public void getRequestFound()
        throws Exception
    {
        HttpGet get = new HttpGet( "http://localhost:" + localPort + "/foo.html" );
        HttpResponse httpResponse = httpClient.execute( get );

        Assert.assertEquals( 200, httpResponse.getStatusLine().getStatusCode() );
        Assert.assertTrue( IOUtils.toString( httpResponse.getEntity().getContent() ).contains( "<div>Foo</div>" ) );

        get = new HttpGet( "http://localhost:" + localPort + "/foo.html" );
        httpResponse = httpClient.execute( get );

        Assert.assertEquals( 200, httpResponse.getStatusLine().getStatusCode() );
        Assertions.assertThat( IOUtils.toString( httpResponse.getEntity().getContent() ).contains( "<div>Foo</div>" ) );

        Header[] date = httpResponse.getHeaders( "Date" );

        Assertions.assertThat( date ).isNotNull().isNotEmpty().hasSize( 1 );

        Header[] contentType = httpResponse.getHeaders( "Content-Type" );

        Assertions.assertThat( contentType ).isNotNull().isNotEmpty().hasSize( 1 );


    }

    @Test
    public void getRequestFoundSubdirectory()
        throws Exception
    {
        HttpGet get = new HttpGet( "http://localhost:" + localPort + "/bar/beer.html" );
        HttpResponse httpResponse = httpClient.execute( get );

        Assert.assertEquals( 200, httpResponse.getStatusLine().getStatusCode() );
        Assertions.assertThat(
            IOUtils.toString( httpResponse.getEntity().getContent() ).contains( "<div>beer</div>" ) );

        get = new HttpGet( "http://localhost:" + localPort + "/bar/beer.html" );
        httpResponse = httpClient.execute( get );

        Assert.assertEquals( 200, httpResponse.getStatusLine().getStatusCode() );
        Assertions.assertThat(
            IOUtils.toString( httpResponse.getEntity().getContent() ).contains( "<div>beer</div>" ) );
    }

    @Test
    public void getRequestNotFound()
        throws Exception
    {

        HttpGet get = new HttpGet( "http://localhost:" + localPort + "/beer.html" );
        HttpResponse httpResponse = httpClient.execute( get );
        Assert.assertEquals( 404, httpResponse.getStatusLine().getStatusCode() );

    }

    @Test
    public void directoryBrowsing()
        throws Exception
    {
        HttpGet get = new HttpGet( "http://localhost:" + localPort + "/bar/" );
        HttpResponse httpResponse = httpClient.execute( get );
        String content = IOUtils.toString( httpResponse.getEntity().getContent() );
        log.info( "directoryBrowsing content: {}", content );
        Assertions.assertThat( content ).contains( "<a href=\"/bar/beer.html\">beer.html/</a>" ).contains(
            "Index of /bar/" );
    }

    @Test
    public void keepAliveTest()
        throws Exception
    {
        String host = "localhost";//"127.0.0.1";//
        int port = localPort;// 8080;//

        String path = "/index.html";

        DefaultHttpClientConnection connection = new DefaultHttpClientConnection();
        final Socket socket = new Socket( host, port );

        socket.setKeepAlive( supportKeepAlive() );

        //socket.setSoTimeout( 999999999 );

        connection.bind( socket, new BasicHttpParams() );

        connection.sendRequestHeader( new HttpGet( path ) );

        connection.flush();

        boolean responseReceived = connection.isResponseAvailable( readTimeOut );

        Assert.assertTrue( responseReceived );

        HttpResponse httpResponse = connection.receiveResponseHeader();

        if ( httpResponse.getStatusLine().getStatusCode() == readTimeOut )
        {

            connection.receiveResponseEntity( httpResponse );

            String content = IOUtils.toString( httpResponse.getEntity().getContent() );
        }

        Assert.assertTrue( connection.isOpen() );

        if ( supportKeepAlive() )
        {
            connection.sendRequestHeader( new HttpGet( path ) );

            connection.flush();

            responseReceived = connection.isResponseAvailable( readTimeOut );

            Assert.assertTrue( responseReceived );

            httpResponse = connection.receiveResponseHeader();

            if ( httpResponse.getStatusLine().getStatusCode() == readTimeOut )
            {
                connection.receiveResponseEntity( httpResponse );

                String content = IOUtils.toString( httpResponse.getEntity().getContent() );
            }

        }
    }

    @Test
    @Ignore( "currently hang" )
    public void socketSending()
        throws Exception
    {
        if ( !supportKeepAlive() )
        {
            return;
        }
        long sleep = 1000;
        try
        {

            Socket socket = new Socket( "localhost", localPort );
            // socket.setReceiveBufferSize(2048);
            // socket.setReceiveBufferSize(2048);
            // socket.getChannel().
            OutputStream out = socket.getOutputStream();
            InputStream in = socket.getInputStream();

            sendTCP( out, in );
            Thread.sleep( sleep );

            sendTCP( out, in );

            out.close();
            in.close();
            socket.close();
        }
        catch ( Exception e )
        {
            throw e;
        }
    }


    public void sendTCP( final OutputStream out, final InputStream in )
        throws Exception
    {

        try
        {

            log.debug( "send rq 1" );
            sendRequest( out );
            log.debug( "read rs1" );
            readReponse( in );
            log.debug( "send rq2" );
            sendRequest( out );

            log.debug( "read rs2" );
            readReponse( in );

        }
        catch ( Exception e )
        {
            log.error( e.getMessage(), e );
            throw e;
        }
    }

    public void readReponse( InputStream in )
        throws Exception
    {
        int cl = 0;
        String contentLength = "Content-Length: ";
        String response = readLine( in );
        while ( response != null )
        {
            log.trace( "readReponse: {}", response );
            if ( StringUtils.equals( CLRFSTR, response ) )
            {

                byte[] b = new byte[cl];

                DataInputStream bin = new DataInputStream( in );
                log.trace( "readReponse#readFully" );
                bin.readFully( b );

                String res = new String( b );
                log.debug( res );

                break;
            }
            if ( response.indexOf( contentLength ) >= 0 )
            {
                // log.error("parse du cl");
                cl = Integer.parseInt( response.trim().substring( contentLength.length() ) );
                // log.error("contentLength="+cl);
            }
            response = readLine( in );
        }
    }

    public static final byte[] CLRF = new byte[]{ (byte) 13, (byte) 10 };

    public static final String CLRFSTR = new String( CLRF );

    public void sendRequest( OutputStream out )
        throws Exception
    {
        try
        {
            out.write( "GET /foo.html HTTP/1.1".getBytes() );

            out.write( CLRF );

            out.write( ( "Host: foo.com" ).getBytes() );
            out.write( CLRF );
            out.write( CLRF );
            out.flush();
        }
        catch ( Exception e )
        {
            log.error( e.getMessage(), e );
            throw e;
        }
    }

    public String readLine( InputStream inputStream )
        throws IOException
    {

        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int ch;
        while ( ( ch = inputStream.read() ) >= 0 )
        {
            log.trace( "readLine: '{}", ch );
            buf.write( ch );
            if ( ch == '\n' )
            {
                break;
            }
        }
        if ( buf.size() == 0 )
        {
            log.trace( "readLine buffer size 0: return null" );
            return null;
        }
        String str = new String( buf.toByteArray() );
        log.trace( "return '{}'", str );
        return str;
    }


}
