package org.olamy.simplehttpserver.launcher;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import org.olamy.simplehttpserver.HttpServerConfiguration;
import org.olamy.simplehttpserver.SimpleHttpServer;
import org.olamy.simplehttpserver.connector.bio.NioHttpConnector;
import org.olamy.simplehttpserver.core.DefaultSimpleHttpServer;
import org.olamy.simplehttpserver.core.handler.StaticFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.PrintStream;

import static org.slf4j.LoggerFactory.getILoggerFactory;


/**
 * @author Olivier Lamy
 */
public class SimpleHttpServerRunner
{

    @Parameter( names = { "-h", "--help" }, description = "Display help information." )
    private boolean printHelp;

    @Parameter( names = { "-p", "--port" }, description = "The Server port (default: 8080." )
    private int port = 8080;

    @Parameter( names = { "-t", "--threads" },
                description = "The number of thread (default: number of available processors)" )
    private int threads = Runtime.getRuntime().availableProcessors();

    @Parameter( names = { "-k", "--keep-alive-timeout" },
                description = "The keep alive connection timeout, in seconds." )
    private int keepAliveTimeOut = 5;

    @Parameter( names = { "-H", "--host" }, description = "The host name." )
    private String host = "localhost";

    @Parameter( names = { "-S", "--sever-name" }, description = "The Server header name to use." )
    private String serverName = "Simple Http Server";

    @Parameter( names = { "-X", "--verbose" }, description = "Produce execution debug output." )
    private boolean verbose;

    @Parameter( names = { "-T", "--trace" }, description = "Produce execution trace output." )
    private boolean trace;

    @Parameter( names = { "-v", "--version" }, description = "Show version information." )
    private boolean printVersion;

    @Parameter( names = { "-V", "--show-version" },
                description = "Show version information WITHOUT stopping the server." )
    private boolean showVersion;


    @Parameter( names = { "-C", "--connector-class" },
                description = "The connector class to use (Default: BIO connector." )
    private String connectorClass = NioHttpConnector.class.getName();

    @Parameter(
        names = { "-s", "--sitedirectory" },
        description = "The directory containing the site to be provided.",
        converter = FileConverter.class )
    private File siteDirectory = new File( System.getProperty( "basedir" ), "simple-website" );

    final SimpleHttpServer simpleHttpServer = new DefaultSimpleHttpServer();

    protected void run( String... args )
    {
        final JCommander jCommander = new JCommander( this );

        jCommander.setColumnSize( 80 );

        jCommander.parse( args );

        if ( printVersion )
        {
            printVersion();
            System.exit( 0 );
        }

        if ( printHelp )
        {
            jCommander.usage();
            System.exit( 0 );
        }

        if ( showVersion )
        {
            printVersion();
        }

        if ( verbose )
        {
            System.setProperty( "logging.level", "DEBUG" );
        }
        else
        {
            System.setProperty( "logging.level", "INFO" );
        }

        if ( trace )
        {
            System.setProperty( "logging.level", "TRACE" );
        }

        // reconfigure logback with the sys props and our file
        final LoggerContext lc = (LoggerContext) getILoggerFactory();

        try
        {
            JoranConfigurator configurator = new JoranConfigurator();
            configurator.setContext( lc );
            lc.reset();
            configurator.doConfigure( getClass().getClassLoader().getResourceAsStream( "logback.xml" ) );
        }
        catch ( JoranException je )
        {
            // no op
        }

        HttpServerConfiguration httpServerConfiguration =
            new HttpServerConfiguration().setHost( this.host ).setServerName( this.serverName ).setHttpConnectorClass(
                this.connectorClass ).setKeepAliveTimeout( this.keepAliveTimeOut * 1000 ).setMaxThreads(
                this.threads ).setPort( this.port ).addRequestHandler( new StaticFileHandler( siteDirectory ) );
        try
        {
            simpleHttpServer.initialize( httpServerConfiguration );
        }
        catch ( Exception e )
        {
            getLogger().error( "Error during server initialize", e );
            System.exit( 1 );
        }

        Runtime.getRuntime().addShutdownHook( new Thread( "shutdown-hook" )
        {

            public void run()
            {
                try
                {
                    simpleHttpServer.shutdown();
                }
                catch ( Exception e )
                {
                    getLogger().error( "error shutdown the server", e );
                }
            }

        } );

        try
        {
            getLogger().info( "Starting simple http server on port {} and serve web directory {}", this.port,
                              this.siteDirectory );
            simpleHttpServer.start();
        }
        catch ( Exception e )
        {
            getLogger().error( "error starting the server", e );
        }
    }

    public static void main( String[] args )
        throws Exception
    {
        new SimpleHttpServerRunner().run( args );
    }

    private static Logger getLogger()
    {
        return LoggerFactory.getLogger( SimpleHttpServerRunner.class );
    }

    private static void printVersion()
    {
        PrintStream out = System.out;

        out.printf( "%s v%s (built on %s)%n", System.getProperty( "project.name" ),
                    System.getProperty( "project.version" ), System.getProperty( "build.timestamp" ) );
        out.printf( "Java version: %s, vendor: %s%n", System.getProperty( "java.version" ),
                    System.getProperty( "java.vendor" ) );


    }
}
